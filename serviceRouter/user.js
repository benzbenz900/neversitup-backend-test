const router = require('express').Router(),
    requireJWTAuth = require('../auth/auth'),
    jwt = require("jwt-simple"),
    loginMiddleWare = require('../auth/loginMiddleWare'),
    db = require('../config/database'),
    SECRET = process.env.SECRET_KEY || "MY_SECRET_KEY";

router.post(["/addUser", "/new"], (req, res) => {
    if (!req.body.username && !req.body.password) {
        res.json({ status: false, message: "Request username and password" })
    } else {
        const users = db.get('users')
        const dataReq = { username: req.body.username, password: req.body.password }

        users.find(dataReq).then((resp) => {
            if (resp.length >= 1) {
                res.json({ status: false, message: "Username already exists" })
            } else {
                users.insert(dataReq).then((resp) => {
                    res.json({ status: true, message: `Add New User Success You ID ${resp._id}` })
                })
            }
        }).catch(error => {
            console.log(error)
            res.json({ status: false, message: "Something went wrong" })
        })

    }
});

router.post("/login", loginMiddleWare, (req, res) => {
    const users = db.get('users')
    const dataReq = { username: req.body.username }
    users.find(dataReq).then((resp) => {
        if (resp.length >= 1) {
            res.json({ status: true, message: "Data Token", data: resp, token: jwt.encode(dataReq, SECRET),url:'/user/profile' })
        }
    })

});

router.get(['/getUserProfile', '/profile'], requireJWTAuth, (req, res) => {
    const users = db.get('users')
    const dataReq = jwt.decode(req.headers.authorization, SECRET)

    users.find(dataReq).then((resp) => {
        if (resp.length >= 1) {
            res.json({ status: true, message: "Success", data: resp })
        } else {
            res.json({ status: false, message: "Username Not Found" })
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: false, message: "Something went wrong" })
    })
})

router.get(["/getMyOrderDetail", "/myorder"], requireJWTAuth, (req, res) => {
    const users = db.get('users')
    const dataReq = jwt.decode(req.headers.authorization, SECRET)

    users.find(dataReq).then((resp) => {
        if (resp.length >= 1) {
            const order = db.get('order')
            order.find({ iduser: resp[0]._id.toString() }).then((resp) => {
                if (resp.length >= 1) {
                    res.json({ status: true, message: "Success", data: resp })
                } else {
                    res.json({ status: false, message: "Order Not Found" })
                }
            }).catch(error => {
                console.log(error)
                res.json({ status: false, message: "Something went wrong" })
            })
        } else {
            res.json({ status: false, message: "Username Not Found" })
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: false, message: "Something went wrong" })
    })
});

module.exports = router
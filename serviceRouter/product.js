const router = require('express').Router(),
    db = require('../config/database')

router.get(['/getProductAll','/getAll','/all'], (req, res) => {
    const users = db.get('product')
    users.find({}).then((resp) => {
        if (resp.length >= 1) {
            res.json({ status: true, message: "Success", data: resp })
        } else {
            res.json({ status: false, message: "Product Not Found" })
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: false, message: "Something went wrong" })
    })
})

router.get(['/getProductDetail','/detail/:id'], (req, res) => {
    const product = db.get('product')
    product.find({ _id: req.params.id }).then((resp) => {
        if (resp.length >= 1) {
            res.json({ status: true, message: "Success", data: resp })
        } else {
            res.json({ status: false, message: "Product Not Found" })
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: false, message: "Something went wrong" })
    })
})

module.exports = router
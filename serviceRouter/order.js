
const router = require('express').Router(),
    requireJWTAuth = require('../auth/auth'),
    db = require('../config/database'),
    jwt = require("jwt-simple"),
    SECRET = process.env.SECRET_KEY || "MY_SECRET_KEY";

router.get(["/getOrderDetail/:id", "/detail/:id"], requireJWTAuth, (req, res) => {
    const users = db.get('users')
    const dataReq = jwt.decode(req.headers.authorization, SECRET)

    users.find(dataReq).then((resp) => {
        if (resp.length >= 1) {
            const order = db.get('order')
            order.find({ _id: req.params.id }).then((resp) => {
                if (resp.length >= 1) {
                    res.json({ status: true, message: "Success", data: resp ,url:`order/cancel/${resp[0]._id}`})
                } else {
                    res.json({ status: false, message: "Order Not Found" })
                }
            }).catch(error => {
                console.log(error)
                res.json({ status: false, message: "Something went wrong" })
            })
        } else {
            res.json({ status: false, message: "Username Not Found" })
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: false, message: "Something went wrong" })
    })
});

router.post(["/cancelOrder/:id", "/cancel/:id"], requireJWTAuth, (req, res) => {
    const users = db.get('users')
    const dataReq = jwt.decode(req.headers.authorization, SECRET)

    users.find(dataReq).then((resp) => {
        if (resp.length >= 1) {
            const order = db.get('order')
            order.update({ _id: req.params.id }, { "$set": { status: 'cancel' } }).then((resp) => {
                if (resp.ok) {
                    res.json({ status: true, message: "Success Cancel Order", data: resp })
                } else {
                    res.json({ status: false, message: "Order Not Found" })
                }
            })
        } else {
            res.json({ status: false, message: "Username Not Found" })
        }
    }).catch(error => {
        console.log(error)
        res.json({ status: false, message: "Something went wrong" })
    })


});

router.post(["/newOrder", "/new"], (req, res) => {
    if (!req.body.dataproduct && !req.body.totalprice) {
        res.json({ status: false, message: "Request dataproduct and totalprice" })
    } else {
        const order = db.get('order')
        const dataReq = { totalprice: req.body.totalprice, dataproduct: req.body.dataproduct, iduser: req.body.iduser, status: 'open' }

        order.insert(dataReq).then((resp) => {
            res.json({ status: true, message: `Add New Order Success You Order ID ${resp._id}`, id: resp._id ,url:`/order/detail/${resp._id}`})
        })
    }
});


module.exports = router
require('dotenv').config()

const app = require("express")();
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.use('/user', require('./serviceRouter/user'))
app.use('/order', require('./serviceRouter/order'))
app.use('/product', require('./serviceRouter/product'))

app.get("/", (req, res) => {
    res.send("Hello World");
});


app.listen(process.env.PORT || 3000);
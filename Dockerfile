FROM node:12.16.3-slim

RUN ln -fs /usr/share/zoneinfo/Asia/Bangkok /etc/localtime && dpkg-reconfigure -f noninteractive tzdata

RUN mkdir -p /app

COPY / /app

WORKDIR /app

RUN npm install

CMD [ "node", "api.js"]
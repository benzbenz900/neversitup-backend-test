const db = require('../config/database');
module.exports = (req, res, next) => {
    if (!req.body.username && !req.body.password) {
        res.json({ status: false, message: "Request username and password" })
    } else {
        const users = db.get('users')
        const dataReq = { username: req.body.username, password: req.body.password }
        users.find(dataReq).then((resp) => {
            if (resp.length >= 1) {
                next();
            } else {
                res.json({ status: false, message: "Username already exists" })
            }
        }).catch(error => {
            console.log(error)
            res.json({ status: false, message: "Something went wrong" })
        })
    }
};

const passport = require("passport"),
    ExtractJwt = require("passport-jwt").ExtractJwt,
    JwtStrategy = require("passport-jwt").Strategy,
    SECRET = process.env.SECRET_KEY || "MY_SECRET_KEY",
    db = require('../config/database'),
    jwtOptions = {
        jwtFromRequest: ExtractJwt.fromHeader('authorization'),
        secretOrKey: SECRET
    },
    jwtAuth = new JwtStrategy(jwtOptions, (payload, done) => {
        const users = db.get('users')
        const dataReq = { username: payload.username }
        users.find(dataReq).then((resp) => {
            if (resp.length >= 1) {
                done(null, { username: resp.username });
            } else {
                done(null, false)
            }
        }).catch(error => {
            done(error)
        })
    });

passport.use(jwtAuth);

module.exports = passport.authenticate("jwt", { session: false });
## setup
``` bash
 npm install //or npm install jwt-simple passport passport-jwt body-parser monk dotenv
 node app.js
```
## Test On Heroku

neversitup-backend-test.herokuapp.com


## Deploy on Heroku
```bash
 heroku login
 heroku git:remote -a neversitup-backend-test #your git heruku name https://git.heroku.com/neversitup-backend-test.git
 heroku config:set DATABASE_URI="mongodb+srv://neversitup:q29jUb3Yz47DXCQX@cluster0.ov9kn.gcp.mongodb.net/neversitup?retryWrites=true&w=majority"
 heroku config:set SECRET_KEY="MY_SECRET_KEY"
 git add .
 git commit -am "make it better"
 git push heroku master
```

## Docker Setup

```bash
# docker build image
 docker build -t neversitup-backend-test .

# serve with hot reload at localhost:3000
 docker run -e DATABASE_URI="mongodb+srv://neversitup:q29jUb3Yz47DXCQX@cluster0.ov9kn.gcp.mongodb.net/neversitup?retryWrites=true&w=majority" -e SECRET_KEY="MY_SECRET_KEY" -p 3000:3000 -d neversitup-backend-test
```

## Docker Compose docker-compose.yml

```bash
    version: "3.3"
    services:
        shiba:
            environment:
                - DATABASE_URI="mongodb+srv://neversitup:q29jUb3Yz47DXCQX@cluster0.ov9kn.gcp.mongodb.net/neversitup?retryWrites=true&w=majority"
                - SECRET_KEY="MY_SECRET_KEY"
            ports:
                - "3000:3000"
            image: neversitup-backend-test

```

## Docker Compose Up

```bash
 docker-compose up -d
```


## URL API

POST /user/new

POST /user/login

GET /user/profile

GET /product/all

GET /product/detail/:id

POST /order/new

GET /order/detail/:id

POST /order/cancel/:id

GET /user/myorder

## Use

POST /user/new

    Payload {
        "username":"Neversitup",
        "password":"Neversitup12345"
    }

    Response {
        "status": true,
        "message": "Add New User Success You ID 5f380d7e16340c1bac2f4c22"
    }

POST /user/login

    Payload {
        "username":"Neversitup",
        "password":"Neversitup12345"
    }

    Response {
        "status": true,
        "message": "Data Token",
        "data": [
            {
                "_id": "5f37e93ac9a0b82ecc443902",
                "username": "Neversitup",
                "password": "Neversitup12345"
            }
        ],
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Ik5ldmVyc2l0dXAifQ.xzGmoHKupTvtHiQz8At1zt2SA_jg6cxLT0UbxqSGtY4",
        "url": "/user/profile"
    }

GET /user/profile

    Header authorization = eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Ik5ldmVyc2l0dXAifQ.xzGmoHKupTvtHiQz8At1zt2SA_jg6cxLT0UbxqSGtY4

    Response {
        "status": true,
        "message": "Success",
        "data": [
            {
                "_id": "5f37e93ac9a0b82ecc443902",
                "username": "Neversitup",
                "password": "Neversitup12345"
            }
        ]
    }

GET /product/all

    Response {
        "status": true,
        "message": "Success",
        "data": [
            {
                "_id": "5f37f28e3a8cc125d5b495be",
                "name": "test Product 1",
                "price": 100,
                "image": "https://picsum.photos/200/300"
            },
            {
                "_id": "5f37f2d33a8cc125d5b495bf",
                "name": "test Product 2",
                "price": 200,
                "image": "https://picsum.photos/200/300"
            },
            {
                "_id": "5f37f2e13a8cc125d5b495c0",
                "name": "test Product 3",
                "price": 300,
                "image": "https://picsum.photos/200/300"
            }
        ]
    }

GET /product/detail/:id || ex: /product/detail/5f37f28e3a8cc125d5b495be

    Response {
        "status": true,
        "message": "Success",
        "data": [
            {
                "_id": "5f37f28e3a8cc125d5b495be",
                "name": "test Product 1",
                "price": 100,
                "image": "https://picsum.photos/200/300"
            }
        ]
    }

POST /order/new

    Payload {
        "iduser":"5f37e93ac9a0b82ecc443902",
        "dataproduct":[{
                "_id": "5f37f28e3a8cc125d5b495be",
                "name": "test Product 1",
                "price": 100,
                "image": "https://picsum.photos/200/300"
            },
            {
                "_id": "5f37f2d33a8cc125d5b495bf",
                "name": "test Product 2",
                "price": 200,
                "image": "https://picsum.photos/200/300"
            }],
        "totalprice":300
    }

    Response {
        "status": true,
        "message": "Add New Order Success You Order ID 5f380e9416340c1bac2f4c23",
        "id": "5f380e9416340c1bac2f4c23",
        "url": "/order/detail/5f380e9416340c1bac2f4c23"
    }

GET /order/detail/:id || ex: /order/detail/5f380a11af73c70f0c2179fd

    Response {
        "status": true,
        "message": "Success",
        "data": [
            {
                "_id": "5f380a11af73c70f0c2179fd",
                "totalprice": 300,
                "dataproduct": [
                    {
                        "_id": "5f37f28e3a8cc125d5b495be",
                        "name": "test Product 1",
                        "price": 100,
                        "image": "https://picsum.photos/200/300"
                    },
                    {
                        "_id": "5f37f2d33a8cc125d5b495bf",
                        "name": "test Product 2",
                        "price": 200,
                        "image": "https://picsum.photos/200/300"
                    }
                ],
                "iduser": "5f37e93ac9a0b82ecc443902",
                "status": "open"
            }
        ],
        "url": "order/cancel/5f380a11af73c70f0c2179fd"
    }

POST /order/cancel/:id || ex: /order/cancel/5f37f7a38e47210ad40566ec

    Response {
        "status": true,
        "message": "Success Cancel Order",
        "data": {
            "n": 1,
            "nModified": 1,
            "opTime": {
                "ts": "6861250640937484289",
                "t": 4
            },
            "electionId": "7fffffff0000000000000004",
            "ok": 1,
            "$clusterTime": {
                "clusterTime": "6861250640937484289",
                "signature": {
                    "hash": "bi6idmkxm+CKPaFyWbsq+4rofJE=",
                    "keyId": "6857244918214033411"
                }
            },
            "operationTime": "6861250640937484289"
        }
    }

GET /user/myorder

    Header authorization = eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6Ik5ldmVyc2l0dXAifQ.xzGmoHKupTvtHiQz8At1zt2SA_jg6cxLT0UbxqSGtY4

    Response {
        "status": true,
        "message": "Success",
        "data": [
            {
                "_id": "5f37f7a38e47210ad40566ec",
                "totalprice": 300,
                "dataproduct": [
                    {
                        "_id": "5f37f28e3a8cc125d5b495be",
                        "name": "test Product 1",
                        "price": 100,
                        "image": "https://picsum.photos/200/300"
                    },
                    {
                        "_id": "5f37f2d33a8cc125d5b495bf",
                        "name": "test Product 2",
                        "price": 200,
                        "image": "https://picsum.photos/200/300"
                    }
                ],
                "iduser": "5f37e93ac9a0b82ecc443902",
                "status": "cancel"
            },
            ...
        ]
    }